clean-debut
============

This is a clean [shopify](http://www.shopify.com/) theme based on [debut] with a small [grunt file](https://github.com/the-working-party/shopify-grunt-tools) that handles all assets.

## Getting Started
This plugin requires Grunt `~1.0.1`

If you haven't used [Grunt](http://gruntjs.com/) before read about it [here](http://css-tricks.com/video-screencasts/130-first-moments-grunt/), [here](http://www.smashingmagazine.com/2013/10/29/get-up-running-grunt/) and [here](https://egghead.io/lessons/gruntjs-introduction-to-grunt).

To run it, install the dependencies via:

```shell
	npm install
```

and run grunt in the root directory

```shell
	grunt
```

## Watch theme
There is also an example YAML file in the theme folder with with you can run the [theme kit](http://shopify.github.io/themekit/) from shopify.

```shell
	cd theme
	theme watch
```
