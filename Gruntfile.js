'use strict';
module.exports = function (grunt) {[
		'grunt-contrib-uglify',
		'grunt-grunticon',
		'grunt-contrib-imagemin',
		'grunt-contrib-concat',
		'grunt-contrib-copy',
		'grunt-newer',
		'grunt-contrib-watch',
		'grunt-contrib-clean',
		'grunt-autoprefixer',
		'grunt-downloadfile',
		'grunt-criticalcss',
		'grunt-svgmin'
	].forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		concat: { //CONCATENATING SCSS FILES
			options: {},
			dev: {
				src: ['lib/scss/setup/*.scss', 'lib/scss/display/*.scss', 'lib/scss/includes/*.scss', 'lib/scss/custom/*.scss'],
				dest: 'theme/assets/theme.scss.liquid'
			},
		},

	/*	autoprefixer: {
		    options: {},
				single_file: {
                src: 'theme/assets/styles.scss.liquid',
                dest: 'theme/assets/styles_prefixed.scss.liquid'
            },
		  }, */

		uglify: { //UGLIFY JS
			shop: {
				options: {
					report: 'min',
				},
				files: {
					'theme/assets/shop.js.liquid': ['lib/js-liquid/*.js.liquid', 'lib/js/libraries/*.js', 'lib/js/*.js'],
				},
			},
		},
		svgmin: {
		    dist: {
		        options: {
		            plugins: [
		                // Don't remove XML declaration (needed to avoid errors creating PNG on Win 7)
		                { removeXMLProcInst: false }
		            ]
		        },
		        files: [{
		            expand: true,
		            cwd: 'source-assets/svg',
		            src: ['*.svg'],
		            dest: 'source-assets/svg-min'
		        }]
		    }
		},

		grunticon: {
		    myIcons: {
		        files: [{
		            expand: true,
		            cwd: 'source-assets/svg-min',
		            src: ['*.svg'],
		            dest: 'compiled-assets/'
		        }],
		        options: {
		        	pngpath: "../assets/",
							datasvgcss: "icons.data.svg.css",
							datapngcss: "icons.data.png.css",
							urlpngcss: "icons.fallback.css",
							cssprefix: '.gicon-',
							enhanceSVG: true,
							corsEmbed: true
		        },
		    },
		},

		imagemin: { //MINIFY IMAGES
			dynamic: {
				options: {
					optimizationLevel: 3,
					cache: false
				},
				files: [{
					expand: true,
					cwd: 'source-assets/images/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'compiled-assets/'
				}],
			},
		},

		copy: { //MOVE IMAGES AND SOURCE ASSETS TO ASSET FOLDER
			images: {
				files: [{
					expand: true,
					flatten: true,
					src: ['compiled-assets/**', 'theme/assets/png/**'],
					dest: 'theme/assets/',
					filter: 'isFile'
				}],
			},
			assets: {
				files: [{
					expand: true,
					flatten: true,
					src: ['source-assets/**', '!source-assets/images/**/*', '!source-assets/svg/**/*'],
					dest: 'theme/assets/',
					filter: 'isFile'
				}],
			},
		},

				/*downloadfile: {
		      files: [
		        {
		          url: 'http://cdn.shopify.com/s/files/1/1011/7718/t/4/assets/timber.scss.css',
		          dest: 'tmp',
		          name: 'all.css'
		        }
		      ]
		  },*/
			/*criticalcss: {
					custom: {
							options: {
									url: "http://jv2-0.myshopify.com/",
									width: 1200,
									height: 900,
									outputfile: "theme/snippets/criticalcss.liquid",
									filename: "tmp/all.css", // Using path.resolve( path.join( ... ) ) is a good idea here
									buffer: 800*1024,
									ignoreConsole: false
							}
					}
			},*/

		clean: {
			js: ['theme/assets/png/**', 'tmp'],
		},

		watch: {
      css: {
        files: ['lib/scss/*/*.scss'],
	        tasks: ['concat:dev']
	      },
	      js: {
	        files: ['lib/js/*.js', 'lib/js/*/*.js', 'lib/js-liquid/*.js.liquid'],
	        tasks: ['uglify']
	      },
			core: {
				files: ['source-assets/timber-assets/*.liquid'],
				tasks: ['newer:copy']
			}
    	}
	});


//TASKS
	grunt.registerTask('default',['svgmin','grunticon:myIcons', 'concat', 'uglify', 'newer:imagemin', 'newer:copy', 'clean', 'watch']);
};
