//Mailchimp submit scripts
jQuery( document ).ready(function( $ ) {
    $('form.mailchimp').submit(function(e){
      var formDiv = $(this);
      //validation for required fields
      var validate = true;
      if(!(formDiv).is('#mc-birthday') && !(formDiv).is('#mc-gigi-giveaway')){
        $.each(formDiv.children('input'), function(){
            if($(this).hasClass('required')){
                var value = $(this).val();
                if(value == ''){
                  $(this).css('background', '#FFEEEE');
                  if(!formDiv.is('#mc-header')){
                  formDiv.children('.mce-responses').html('<div class="response" id="mce-success-error">Please enter all required fields</div>').fadeIn().delay(6000).fadeOut(400);
                  }
                  validate = false;
                  e.preventDefault();
                  return false;
                }
              }
            });
        }
      if(validate == false){
        return false;
      }
      /*** BIRTHDAY FORM VALIDATION ***/
      if((formDiv).is('#mc-birthday')){
        //check that month and day have values
        var month = $('select#month option:selected').val(),
            day = $('select#day option:selected').val();
            if(month == '' || day == ''){
              $(formDiv).children('.mce-responses').html('<div class="response" id="mce-success-error">Please select both month and day for your birthday.</div>').fadeIn();
              $(formDiv).children('.mce-responses').delay(5000).fadeOut(400);
            e.preventDefault();
            return false;
            }
        if($('input[name=FNAME]').length){
            var name = $('input[name=FNAME]').val();
            if(name == ''){
              $(formDiv).children('.mce-responses').html('<div class="response" id="mce-success-error">Please enter your first name.</div>').fadeIn();
              e.preventDefault();
              return false;
            }
        }
      }
      /***GIGI FORM VALIDATION***/
      if((formDiv).is('#mc-gigi-giveaway')){
            var instagram = $('#mce-instagram-gigi-giveaway').val();
            if(instagram == ''){
              formDiv.children('.mce-responses').html('<div class="response" id="mce-success-error">Please enter your Instagram Username.</div>').fadeIn();
              e.preventDefault();
              return false;
            }
            var email = $('#mce-email-gigi-giveaway').val();
            if(email == ''){
              $(formDiv).children('.mce-responses').html('<div class="response" id="mce-success-error">Please enter your Email address.</div>').fadeIn();
              e.preventDefault();
              return false;
        }
      }
      e.preventDefault();
        var data = $(this).serialize();
        $.post('https://mc-julievos.rhcloud.com/mc-api.php', data, function (response) {
            var res = $.parseJSON(response);
            if(res.referrer == 'retail_gigi-giveaway'){
              if (res.status != 'subscribed') {
                  $(formDiv).children('.mce-responses').html('<div class="response" id="mce-error-response">Error. This address is already subscribed or is invalid.</div>').fadeIn();
              } else if (res.status == 'subscribed') {
                $('#mc-gigi-giveaway').remove();
                $('#gigi-steptwo').show();
              }
            }
            else if(res.referrer != 'retail_birthday' && res.referrer != 'retail_birthday_update'){
              //console.log(res);
              if (res.status != 'subscribed') {
                  $(formDiv).children('.mce-responses').html('<div class="response" id="mce-error-response">Error. This address is already subscribed or is invalid.</div>').fadeIn();
                  $(formDiv).children('.mce-responses').delay(5000).fadeOut(400);
              } else if (res.status == 'subscribed') {
                  if((formDiv).is('#mc-popup')){
                    $('#mc-popup-window .box').addClass('subscribe');
                    $('#mc-popup-window').delay(6000).fadeOut(400);
                  } else {
                    if((formDiv).is('#mc-birthday')){
                      $(formDiv).html('<div class="response" id="mce-success-response">Thank you! You are now subscribed to Julie Vos.<br /><a href="/" class="btn">Start Shopping</a></div>').fadeIn();
                    } else {
                      $(formDiv).find('input, button').remove();
                      $(formDiv).children('.mce-responses').html('<div class="response" id="mce-success-response">Success! You have been subscribed to Julie Vos.</div>').fadeIn();
                    }
                  }
              }
            } else {//referrer is retail_birthday or retail_birthday_update
              if (res.status != 'subscribed') {
                  $(formDiv).children('.mce-responses').html('<div class="response" id="mce-error-response">Error. Your information has not been updated. Please contact us for help.</div>').fadeIn();
              } else {
                  $(formDiv).html('<div class="response" id="mce-success-response">Thank you! Your information has been updated.<br /><a href="/" class="btn">Start Shopping</a></div>').fadeIn();
              }
            }
         });

    });
     });