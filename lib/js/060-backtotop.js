function backToTop(docHeight){
  var scrollHeight = $(document).scrollTop();
  var winHeight = $(window).height();
  if(! $('#backToTop').length){
    $('body').append('<div id="backToTop"><div class="gicon-up-arrow"></div></div>');
  }
  //console.log(scrollHeight);
  if(scrollHeight > 1200){
    if($('#backToTop').is(":hidden")){
      $('#backToTop').fadeIn(300);
    }
  } else if ((scrollHeight <= 1200) && (scrollHeight > 0)) {
    if(! $('#backToTop').is(":hidden")){
      $('#backToTop').fadeOut(300);
    }
  }
}

$('body').on('click', '#backToTop', function(){
  $(document).scrollTop(function(){
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
  });
});
