//create history state with id of element as parameter
$(function() {
  $(".product-container").on('click', 'a.product-link', function() {
	var currURL = '//' + location.host + location.pathname;
	var paramID = $(this).parents('.product-item').data('id');
	var urlState = currURL+'?p='+paramID;
	history.pushState({}, '', urlState);
//  console.log(paramID);
  //return false;
	});
});
//Detect parameter and scrollto element, if not in view load more

function scrollToMyItem() {
		//see if it exists
    var itemToFind = getUrlParameter('p');
    if (itemToFind !== '' && itemToFind !== null && itemToFind !== undefined && itemToFind !== 'null' && itemToFind !== 'undefined') {//if there is P parameter
  //  console.log(itemToFind);
        var product = $(".product-item[data-id="+itemToFind+"]");

    		  if($(product).length > 0) {
        			$('html, body').animate({
        				scrollTop: $(product).offset().top - 100
        			}, 0);
            } else {
              $('html, body').scrollTop($(document).height());
                  //console.log('load more');
    			   };

          } else {
        return false;
      }
		} //end scrollToItem function
