window.lazySizesConfig = window.lazySizesConfig || {};

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//check if mega menu is off screen, if so, push to left side
/*
$('ul.site-nav > li:first-child').hover(function(){
    if($('#mega-dropdown').is(':off-left')){
      var offset = $('#mega-dropdown').offset().left;
      var width = $('#mega-dropdown').width();
      offset = (width * .45) + offset;
      $('#mega-dropdown').css({
        '-ms-transform': 'translateX(-'+offset+'px)',
        '-webkit-transform': 'translateX(-'+offset+'px)',
        'transform': 'translateX(-'+offset+'px)',
        '-moz-transform': 'translateX(-'+offset+'px)'
        });
    } else {
      $('#mega-dropdown').css({
        '-ms-transform': 'translateX(-45%)',
        '-webkit-transform': 'translateX(-45%)',
        'transform': 'translateX(-45%)',
        '-moz-transform': 'translateX(-45%)'
      });
    }
});
*/
// * AJH 4/12/16 disabled with new content collection template
// --------------------------------------------------------------------------

//alt image remove/show on mobile
// $(window).setBreakpoints({
//     distinct: true,
//     breakpoints: [
//         320,
//         480,
//         768
//     ]
// });

// $(window).bind('enterBreakpoint320',function() {
//     showHideAltCollectionImage('hide');
// });

// $(window).bind('exitBreakpoint320',function() {
//     showHideAltCollectionImage('show');
// });

// function showHideAltCollectionImage(value){
//   var image = $('.alt-image-container.small-hide');
//   if (value == 'show'){
//     $(image).remove();
//     $('.grid--has-alt-images').prepend(image);
//   }
//   else if (value == 'hide'){
//       $(image).remove();
//       $('body').append(image);
//   }
// }

//
// Alt image container size on collection grid
// if($('.alt-image-container').length){
//   $(window).on('load resize', function(){
//     alt_image_container_height();
//   })

//   function alt_image_container_height(){
//     var image_height;
//     if($('.sidebar').is(':visible')){
//     var first_item = $('.product-container .product-item').eq(0).offset().top;
//     var fifth_item = $('.product-container .product-item').eq(4).offset().top;
//     image_height = fifth_item - first_item - 30;
//   } else {
//     image_height = $('.product-container .product-item').eq(0).height();
//   }
//     $('.alt-image-container').height(image_height);
//   }
// }

// --------------------------------------------------------------------------


// sticky footer
function setStickyFooter() {
  var footerHeight = $('.site-footer').outerHeight();
  $('#footer-spacer').height(footerHeight);
  $('#PageWrap').css('margin-bottom', -footerHeight);
}

$(document).ready(function(){

  $(window).load(function() { setStickyFooter() });
  $(window).resize( $.throttle( 250, setStickyFooter ) );

//determine site type
  var storeType = $('#storeType').text().replace(/\s+/g,'');

//menu and sub menu accordioning
function jv_openMenu(link, parentSelector, e){
  if(!$(link).parents(parentSelector).hasClass('open')){
    $(link).parents(parentSelector).addClass('open');
    } else {
      $(link).parents(parentSelector).removeClass('open');
    }
  if(e != '' && e != undefined && e != null){
    e.preventDefault();
  }
}
$('ul.sub-menu.has-sub-sub li.label > a').click(function(e){
  jv_openMenu(this, '.has-sub-sub', e);
});
$('li.has-dropdown > a').click(function(e){
  jv_openMenu(this, '.has-dropdown', e);
});
//check for active links and open the menus
if($('ul.sidebar_list li.filter--active').length > 0){
  $('ul.sidebar_list li.filter--active').each(function(){
    jv_openMenu(this, '.has-dropdown');
    jv_openMenu(this, '.has-sub-sub');
  })
}

//swatch label names and SKUs
  //Name of swatches appear to the right of the swatch label
if($('.swatch').length > 0){
  var swatch_onLoad = $('.swatch input:checked');
  var swatch_optionOnLoad = $(swatch_onLoad).prop('name');
  var swatch_optionLabelText = $('label[for="productSelect-'+swatch_optionOnLoad+'"]').html();
  if(storeType == 'wholesale'){
      var productSku = $('select#productSelect').find(":selected").data('sku');
      productSku = ' <span class="wholesale sku"><span class="thin">|</span> SKU: <span class="thin">'+productSku+'</span></span>';
    } else {
      productSku = '';
    }
  $('label[for="productSelect-'+swatch_optionOnLoad+'"]').html(swatch_optionLabelText + ': <span class="thin">'+$(swatch_onLoad).val()+'</span>'+productSku);
}


  $('select#productSelect').on('change', function(){
      if(storeType == 'wholesale'){
        var productSku = $('select#productSelect').find(":selected").data('sku');
        productSkuNoWrap = '<span class="thin">|</span> SKU: <span class="thin">'+productSku+'</span>';
        productSku = ' <span class="wholesale sku"><span class="thin">|</span> SKU: <span class="thin">'+productSku+'</span></span>';
          $('.wholesale.sku').html(productSkuNoWrap);
      } else {
        productSku = '';
      }
      if($('.swatch input:checked').val() != ''){
          var option = $('.swatch input:checked').prop('name');
          $('label[for="productSelect-'+option+'"]').html(swatch_optionLabelText + ': <span class="thin">'+$('.swatch input:checked').val()+'</span>'+productSku);
        }
    });

//Swatches on collection page update grid item image when clicked
$('.main-content').on('change', '.swatch.collection input' ,function(){
  if($(this).val() != ''){
  var labelDiv = $(this).parent('.swatch-element').children('label');
  var prodID = $(this).parent('.swatch-element').data('productid');
  var newThumb = $(labelDiv).data('zoom');
  var newThumbAlt = $(labelDiv).data('alt');
  var image = $('.grid__item.collection[data-id='+prodID+'] a.product-link img.product-thumb');
  //console.log(newThumb);
  $(image).replaceWith('<img class="product-thumb" src="'+newThumb+'" alt="'+newThumbAlt+'"/>');
  $(image).parents('a.product-link').attr('title', newThumbAlt);
  }
})

//make size selector inline-block if it's the second select box
if($('#productSelect-option-1').length > 0){
  $('#productSelect-option-1').parents('.selector-wrapper').css('display', 'block').addClass('gutter-right-half');
  $('#productSelect-option-1').show();
}

//show selector if there's only one and it's for size or style
//console.log($('#productSelect-option-0').parents('.selector-wrapper').children('label').text().toLowerCase());
var sizeStyleLabel = $('#productSelect-option-0').parents('.selector-wrapper').children('label').text().toLowerCase();
if( sizeStyleLabel == 'size' || sizeStyleLabel == 'style' || sizeStyleLabel == 'amount'){
  $('#productSelect-option-0').parents('.selector-wrapper').css('display', 'inline-block').addClass('gutter-right-half');
  $('#productSelect-option-0').show();
}
//hide label if there's only one variant to pick from
$(window).load(function(){
  $('.single-option-selector').each(function(){
    if($(this).children('option').length > 1){
      $(this).parents('.selector-wrapper').show();
    }
  })
})

//SLICK product thumbs init
  $('#ProductThumbs').slick({
      slidesToShow: 3,
      lazyLoad: 'ondemand',
        slidesToScroll: 1,
        responsive: [
         {
           breakpoint: 768,
           settings: {
             slidesToShow: 2,
             slidesToScroll: 2
           }
         },
         {
           breakpoint: 480,
           settings: {
             slidesToShow: 1,
             slidesToScroll: 1
           }
         }
       ]
  });
  //SLICK Carousel Large init
    $('#Carousel').slick({
        lazyLoad: 'ondemand',
        slidesToScroll: 1,
        adaptiveHeight: true,
          responsive: [
           {
             breakpoint: 768,
             settings: {
               slidesToShow: 1,
               slidesToScroll: 1
             }
           },
           {
             breakpoint: 480,
             settings: {
               slidesToShow: 1,
               slidesToScroll: 1
             }
           }
         ]
    });


  //OPEN TOP DRAWER
  $('.openTopDrawer').click(function(){
      var drawerTarget = $(this).data('target');
      if($('.topDrawer.open.'+drawerTarget).hasClass('open')){
        $('.topDrawer.open.'+drawerTarget).removeClass('open');
      } else {
        $('.topDrawer.open').removeClass('open');//close all open top drawers
        $('.topDrawer.'+drawerTarget).addClass('open');
        $('body,html').animate({
          scrollTop: 0
        }, 800);
      }
      return false;
  });

  $('.main-content').on('load', 'img.zoomed', function(){
      $(this).show();
      //console.log('loaded '+this);
  });

  	//load image if requested on hover
  	$('.grid__item.product-container').on('mouseenter', 'a.product-link', function(){
  		if($(this).find('img.zoomed').length === 0){
  		var hover_url = $(this).children('img').data('hover');
  			if (hover_url != 'undefined' && hover_url != undefined && hover_url != '' && hover_url != null) {
  				$(this).append( "<img class='zoomed' src='"+hover_url+"'>" );
  			   }
      		}
      });
});
$(window).load(function() {
    // this will fire after the entire page is loaded, including images
  jv_load_hover_images();
});

function jv_load_hover_images(){
  $('a.product-link').each(function(){

  if($(this).find('img.zoomed').length === 0){
    //if this is not an zoomed image and this doesn't have a zoomed child already
      var hover_url = $(this).find('img').data('hover');

      if (hover_url != 'undefined' && hover_url != undefined && hover_url != '' && hover_url != null) {
        $(this).append( "<img class='zoomed' src='"+hover_url+"'>" );
            }
        }
    });
}
