$(function(){
  var subMenuHeight = 84;
  $('li.has-mega-dropdown').mouseenter(function(){
    closeDropdown();
    openMega($(this));
  })
  $('li.no-dropdown').mouseenter(function(){
    closeMega();
    closeDropdown();
  })
  $('li.site-nav--has-dropdown').mouseenter(function(){
    if(!$(this).hasClass('has-mega-dropdown')){
    closeMega();
    closeDropdown();
    openDropdown($(this));
    }
  })
  $('.site-nav').on('click', 'li.has-mega-dropdown.open', function(e){
    e.preventDefault();
    closeMega();
  })
  $('.site-nav').on('click', 'li.site-nav--has-dropdown.open', function(e){
    e.preventDefault();
    closeDropdown();
  })

  $('ul.mega-nav li a').click(function(e){
    var link = $(this).data('link');
    if(link != undefined){
      e.preventDefault();
      megaSubSwitch($(this));
      openMega();
    } else {
      return;
    }
  })

  $('#mega-dropdown').mouseleave(function(){
    closeMega();
  })
  $('ul.site-nav__dropdown').mouseleave(function(){
    closeDropdown();
  })

  $('.mega-items.collection-type ul li').hover(function(){
    var imgUrl = $(this).data('hoverimg');
    $(this).parents('.mega-items').find('img').attr('src', imgUrl);
  })

  function openMega($source){
    if($source != undefined){
    $source.addClass('open');
    $source.children('a').addClass('hover');
    }
    $('.mega-nav').height($("#mega-dropdown").get(0).scrollHeight - 56);
    $('#mega-dropdown').height($("#mega-dropdown").get(0).scrollHeight);
  }
  function closeMega(){
    $('#mega-dropdown').height(0);
    $('li.has-mega-dropdown.open a.hover').removeClass('hover');
    $('li.has-mega-dropdown.open').removeClass('open');
  }
  function openDropdown($source){
    var target = $source.attr('data-src');
    var $submenu = $('ul.site-nav__dropdown[data-target="'+target+'"]');
    $source.addClass('hover');
    $submenu.addClass('open');
  }
  function closeDropdown(){
    var $closeDropdown = $('ul.site-nav__dropdown.open');
    $closeDropdown.removeClass('open');
    $closeDropdown.on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
      $('li.site-nav--has-dropdown.hover').removeClass('hover');
      $(this).off("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");
    });
  }
  function megaSubSwitch($link){
    $('.mega-items').removeClass('show-items').addClass('hide-items');
    $('.mega-nav li.active').removeClass('active');
    $link.parents('li').addClass('active');
    var link = $link.data('link');
    $('.mega-items[data-target="'+link+'"]').removeClass('hide-items').addClass('show-items');
  }

  var scrollHeightToStick = $('#mega-dropdown').offset().top;
  $(window).scroll(function() {
    stickyMenu();
  });
  $(window).setBreakpoints({
    distinct: true,
    breakpoints: [
        768
      ]
});

var allowSticky = false;
$(window).bind('enterBreakpoint768',function() {
    allowSticky = true;
});
$(window).bind('exitBreakpoint768',function() {
    allowSticky = false;
});
  function stickyMenu(){
    var scrollTop = $(window).scrollTop();
    if (scrollTop > scrollHeightToStick && !$('.site-header').hasClass('menu-sticky') && allowSticky) {
        var headerHeight = $('.site-header').outerHeight();
        $('.site-header').addClass('menu-sticky is-moved-by-drawer');
        $('.main-content').css('padding-top', headerHeight);
        $('.site-header').insertAfter($('#CartDrawer'));
    } else if(scrollTop <= scrollHeightToStick){
        $('.site-header').removeClass('menu-sticky is-moved-by-drawer');
        $('.main-content').css('padding-top', 0);
        $('.site-header').insertBefore($('.main-content'));
    }
  }
  //show or hide cart dot
  cartDot();
  function cartDot(){
    if(parseInt($('#CartCount').text()) > 0){
      $('.cartdot').show();
    } else {
      $('.cartdot').hide();
    }
  }
  $("#CartCount").on('DOMSubtreeModified', function () {
    cartDot();
  });
})